#include <QtGui/QApplication>
#include "superficie.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Superficie *sup = new Superficie;
    a.setActiveWindow(sup);
    sup->show();

    return a.exec();
}
