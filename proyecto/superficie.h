#ifndef SUPERFICIE_H
#define SUPERFICIE_H

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

//#include <QtOpenGL>
#include <QGLWidget>
//#include <GL/glu.h>
#include <QTimer>
#include <QKeyEvent>
#include <math.h>

class Superficie : public QGLWidget
{
    Q_OBJECT

public:
    Superficie();

    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    void timeOut();

protected slots:
    void timeOutSlot();
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);

private:
    QTimer *m_timer;
    GLfloat anglex;
    GLfloat angley;
    void frontSurfaceControl();
    void backSurfaceControl();
    void drawWireSurface();
    void drawSolidSurface();
    void drawSurfaceFront();
    void drawSurfaceBack();
    void drawWireSphere(GLdouble radius, GLint slices, GLint stacks);
    void drawSolidSphere(GLdouble radius, GLint slices, GLint stacks);
    void drawWireTorus(GLdouble innerRadius, GLdouble outerRadius,GLint nsides, GLint rings);
    void drawSolidTorus(GLdouble innerRadius, GLdouble outerRadius, GLint nsides, GLint rings);
    void doughnut(GLfloat r, GLfloat R, GLint nsides, GLint rings);
    void drawWireCilinder(GLfloat bottomRadius, GLfloat topRadius, GLfloat length,GLint nsides, GLint rings);
    void drawSolidCilinder(GLfloat bottomRadius, GLfloat topRadius, GLfloat length,GLint nsides, GLint rings);
    void drawBox(GLfloat size, GLenum type);
    void drawWireCube(GLdouble size);
    void drawSolidCube(GLdouble size);
    void drawSpace();
    void loadTextures();
    /*GLfloat* normalPlane(GLfloat x1, GLfloat y1, GLfloat z1,
                         GLfloat x2, GLfloat y2, GLfloat z2,
                         GLfloat x3, GLfloat y3, GLfloat z3);*/
    GLUnurbsObj *theNurb;
    GLUquadricObj *quadObj;
    GLfloat ctlpoints[4][4][3];
    GLfloat ctlpointsBack[4][4][3];
    GLfloat z;
    GLfloat knots[8];
    GLfloat radio;
    GLfloat y;
    GLuint texture[3];
    GLfloat Lx;
    GLfloat light_position[4];
};

#endif // SUPERFICIE_H
