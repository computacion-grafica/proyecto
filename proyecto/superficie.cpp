#include "superficie.h"
#include <QDebug>

Superficie::Superficie()
{
    //m_timer = new QTimer(this);
    //connect(m_timer,SIGNAL(timeout()), this, SLOT(timeOutSlot()));
    //m_timer->start(100); //100 milisegundo
    anglex=0.0f;
    angley=0.0f;
    z=-15.0f;
    for(int i=0;i<4;i++)
    {
       knots[i] = 0.0;
       knots[i+4] = 1.0;
    }
    Lx=0.0;
}

void Superficie::initializeGL()
{
    loadTextures();

    glClearStencil(0x0);
    glEnable(GL_STENCIL_TEST);

    glShadeModel(GL_SMOOTH);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);  // Enables Depth Testing
    glDepthFunc(GL_LEQUAL);  // The Type Of Depth Test To Do
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // Really Nice Perspective

    frontSurfaceControl();
    backSurfaceControl();

    theNurb = gluNewNurbsRenderer();
    gluNurbsProperty(theNurb, GLU_SAMPLING_TOLERANCE, 25.0);

    quadObj = gluNewQuadric();

    //las luces son blancas.
    GLfloat light_ambient[] = { 0.5, 0.5, 0.5, 1.0 };
    GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    light_position[0] = 10.0;
    light_position[1] = 5.0;
    light_position[2] = 5.0;
    light_position[3] = 1.0;//luz direccional
    //GLfloat lmodel_ambient[] = { 0.4, 0.4, 0.4, 1.0 };

    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    //glLightfv(GL_LIGHT0, GL_POSITION, light_position);//configuro posicion

    //Materiales para oro
    GLfloat mat_ambiente[] = {0.329412, 0.223529, 0.027451, 1.0};
    GLfloat mat_diffuse[] = {0.780392, 0.568627, 0.113725, 1.0};
    GLfloat mat_specular[] = {0.992157, 0.941176, 0.807843, 1.0};
    GLfloat mat_shininess[] = { 27.8974 };

    //creo el comportamiento del material
    glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambiente);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
    //glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

    //habilido iluminacion y la luz 0
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    glEnable(GL_NORMALIZE);
}

void Superficie::resizeGL(int width, int height)
{
    if (height==0) // Prevent A Divide By Zero By
    {
        height=1;  // Making Height Equal One
    }

    glViewport(0, 0, width, height); // Reset The Current Viewport

    //The projection matrix is responsible for adding perspective to our scene
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity(); // Reset The Projection Matrix
    // Calculate The Aspect Ratio Of The Window
    gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void Superficie::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();
    glTranslatef(0.0f,0.0f,z);

    glPushMatrix();
        glRotatef(Lx, 0.0f,1.0f,0.0f);
        glLightfv (GL_LIGHT0, GL_POSITION, light_position);
    glPopMatrix();

    //surface
    glPushMatrix();
        glRotatef(angley,1.0f,0.0f,0.0f);
        glRotatef(anglex, 0.0f, 1.0f, 0.0f);
        //glRotatef(angle, 1.0f, 0.0f, 0.0f);
        glScalef(0.5, 0.5, 0.5);
        //glColor3f(1.0f,0.0f,0.0f);
        //drawWireSurface();
        drawSolidSurface();
    glPopMatrix();

    //body
    glPushMatrix();
        glRotatef(angley,1.0f,0.0f,0.0f);
        glRotatef(anglex, 0.0f, 1.0f, 0.0f);
        glScalef(0.5, 0.5, 0.5);
        //glRotatef(angle,1.0f,0.0f,0.0f);
        glTranslatef(0.0f,-3.0f,0.0f);
        GLdouble eqn[4] = {0.0, -1.0, 0.0, 0.0};
        glClipPlane(GL_CLIP_PLANE0, eqn);
        glEnable(GL_CLIP_PLANE0);
        //glColor3f(1.0f,0.0f,0.0f);
        //drawWireSphere(3.1,30,16);
        drawSolidSphere(3.1,30,16);
        glDisable(GL_CLIP_PLANE0);
    glPopMatrix();

    //cabeza
    glPushMatrix();
        glRotatef(angley,1.0f,0.0f,0.0f);
        glRotatef(anglex, 0.0f, 1.0f, 0.0f);
        glScalef(0.5,0.5,0.5);
        //glRotatef(angle,1.0f,0.0f,0.0f);
        glTranslatef(0.0f,3.0f,0.0f);
        //glColor3f(1.0f,0.0f,0.0f);
        //drawWireSphere(1.5,20,16);
        drawSolidSphere(1.5,20,16);

        GLfloat xc = 1.2f;
        GLfloat yc = 1.2f;
        GLfloat zc = 1.2f;
        GLfloat rc = 1.25f;

        //front right
        glPushMatrix();
            //glScalef(0.5,0.5,0.5);
            glTranslatef(xc,yc,zc);
            //glColor3f(1.0f,0.0f,0.0f);
            //drawWireSphere(rc,15,16);
            drawSolidSphere(rc,15,16);
        glPopMatrix();

        //front left
        glPushMatrix();
            //glScalef(0.5,0.5,0.5);
            glTranslatef(-xc,yc,zc);
            //glColor3f(1.0f,0.0f,0.0f);
            //drawWireSphere(rc,15,16);
            drawSolidSphere(rc,15,16);
        glPopMatrix();

        //back right
        glPushMatrix();
            //glScalef(0.5,0.5,0.5);
            glTranslatef(xc,yc,-zc);
            //glColor3f(1.0f,0.0f,0.0f);
            //drawWireSphere(rc,15,16);
            drawSolidSphere(rc,15,16);
        glPopMatrix();

        //back left
        glPushMatrix();
            //glScalef(0.5,0.5,0.5);
            glTranslatef(-xc,yc,-zc);
            //glColor3f(1.0f,0.0f,0.0f);
            //drawWireSphere(rc,15,16);
            drawSolidSphere(rc,15,16);
        glPopMatrix();

        //top sphere
        glPushMatrix();
            //glScalef(0.5,0.5,0.5);
            glTranslatef(0,2*yc+0.5,0);
            //glColor3f(1.0f,0.0f,0.0f);
            //drawWireSphere(rc,15,16);
            drawSolidSphere(rc,15,16);
        glPopMatrix();
    glPopMatrix();

    //ring
    glPushMatrix();
        glRotatef(angley,1.0f,0.0f,0.0f);
        glRotatef(anglex, 0.0f, 1.0f, 0.0f);
        glScalef(0.5,0.5,0.5);
        glTranslatef(0.0f,-3.0f,0.0f);
        glRotatef(90.0f,1.0f,0.0f,0.0f);
        glScalef(2.7f,2.7f,1.0f);
        //drawWireTorus(0.275, 0.85, 8, 40);
        drawSolidTorus(0.275, 0.85, 8, 40);
    glPopMatrix();

    //base
    glPushMatrix();
        glRotatef(angley,1.0f,0.0f,0.0f);
        glRotatef(anglex, 0.0f, 1.0f, 0.0f);
        glScaled(0.5,0.5,0.5);
        glTranslated(0.0f,-6.5f,0.0f);
        glRotatef(-90.0f,1.0f,0.0f,0.0f);
        drawWireCilinder(2.0f,1.0f,1.5f,10,5);
        //drawSolidCilinder(2.0f,1.0f,1.5f,20,30);
    glPopMatrix();

    //Espacio
    glPushMatrix();
        glDisable(GL_LIGHTING);//Si se deja activo las texturas quedan con luz (oro)
        glEnable(GL_TEXTURE_2D);
        glScaled(0.5, 0.5, 0.5);
        glTranslated(0.0f,0.0f,-5.0f);
        glRotated(0.0f,1.0f,0.0f,0.0f);
        glRotated(0.0f,0.0f,1.0f,0.0f);
        drawSpace();
        glDisable(GL_TEXTURE_2D);
        glEnable(GL_LIGHTING);
    glPopMatrix();

    glFlush();
}

void Superficie::timeOut()
{
    //angle+=0.5f;
    //updateGL();
}

void Superficie::timeOutSlot()
{
    this->timeOut();
}

void Superficie::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
            case Qt::Key_Escape:
                    //QCoreApplication::exit();
                    break;

            case Qt::Key_Left:
                anglex -= 0.7;
                break;
            case Qt::Key_Right:
                anglex += 0.7;
                break;
            case Qt::Key_Up:
                 angley += 0.7;
                break;
            case Qt::Key_Down:
                 angley -= 0.7;
                 break;
    }
    updateGL();
}

void Superficie::frontSurfaceControl()
{
    //Puntos control v=0
    //M
    ctlpoints[0][0][0] = -3.0;
    ctlpoints[0][0][1] = -3.0;
    ctlpoints[0][0][2] = 0.0;

    //I
    ctlpoints[1][0][0] = -0.75;
    ctlpoints[1][0][1] = -1.5;
    ctlpoints[1][0][2] = 0.0;

    //E
    ctlpoints[2][0][0] = -0.75;
    ctlpoints[2][0][1] = 1.5;
    ctlpoints[2][0][2] = 0.0;

    //A
    ctlpoints[3][0][0] = -1.5;
    ctlpoints[3][0][1] = 3.0;
    ctlpoints[3][0][2] = 0.0;


    //v=1

    //N
    ctlpoints[0][1][0] = -2.0;
    ctlpoints[0][1][1] = -3.0;
    ctlpoints[0][1][2] = 4.0;

    //J
    ctlpoints[1][1][0] = -0.5;
    ctlpoints[1][1][1] = -1.5;
    ctlpoints[1][1][2] = 1.0;

    //F
    ctlpoints[2][1][0] = -0.5;
    ctlpoints[2][1][1] = 1.5;
    ctlpoints[2][1][2] = 1.0;

    //B
    ctlpoints[3][1][0] = -1.0;
    ctlpoints[3][1][1] = 3.0;
    ctlpoints[3][1][2] = 2.0;

    //v=2

    //O
    ctlpoints[0][2][0] = 2.0;
    ctlpoints[0][2][1] = -3.0;
    ctlpoints[0][2][2] = 4.0;

    //K
    ctlpoints[1][2][0] = 0.5;
    ctlpoints[1][2][1] = -1.5;
    ctlpoints[1][2][2] = 1.0;

    //G
    ctlpoints[2][2][0] = 0.5;
    ctlpoints[2][2][1] = 1.5;
    ctlpoints[2][2][2] = 1.0;

    //C
    ctlpoints[3][2][0] = 1.0;
    ctlpoints[3][2][1] = 3.0;
    ctlpoints[3][2][2] = 2.0;

    //v=3
    //P
    ctlpoints[0][3][0] = 3.0;
    ctlpoints[0][3][1] = -3.0;
    ctlpoints[0][3][2] = 0.0;

    //L
    ctlpoints[1][3][0] = 0.75;
    ctlpoints[1][3][1] = -1.5;
    ctlpoints[1][3][2] = 0.0;

    //H
    ctlpoints[2][3][0] = 0.75;
    ctlpoints[2][3][1] = 1.5;
    ctlpoints[2][3][2] = 0.0;

    //D
    ctlpoints[3][3][0] = 1.5;
    ctlpoints[3][3][1] = 3.0;
    ctlpoints[3][3][2] = 0.0;
}

void Superficie::backSurfaceControl()
{
    for(int i=0; i<4; i++)
        for(int j=0; j<4; j++)
        {
            ctlpointsBack[i][j][0]=ctlpoints[i][j][0];
            ctlpointsBack[i][j][1]=ctlpoints[i][j][1];
            ctlpointsBack[i][j][2]=ctlpoints[i][j][2]*-1;
        }
}

void Superficie::drawSurfaceFront()
{

    gluBeginSurface(theNurb);
        gluNurbsSurface(theNurb,
                        8, knots, 8, knots,
                        4 * 3, 3, &ctlpoints[0][0][0],
                        4, 4, GL_MAP2_VERTEX_3);
        gluNurbsSurface(theNurb,
                        8, knots, 8, knots,
                        4 * 3, 3, &ctlpoints[0][0][0],
                        4, 4, GL_MAP2_NORMAL);
    gluEndSurface(theNurb);

    //draw control points
    /*glPointSize(5.0);
    //glDisable(GL_LIGHTING);
    glColor3f(0.0, 1.0, 0.0);
    glBegin(GL_POINTS);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                glVertex3f(ctlpoints[i][j][0],
                           ctlpoints[i][j][1], ctlpoints[i][j][2]);
            }
        }
    glEnd();*/
}

void Superficie::drawSurfaceBack()
{
    gluBeginSurface(theNurb);
        gluNurbsSurface(theNurb,
                        8, knots, 8, knots,
                        4 * 3, 3, &ctlpointsBack[0][0][0],
                        4, 4, GL_MAP2_VERTEX_3);
        gluNurbsSurface(theNurb,
                        8, knots, 8, knots,
                        4 * 3, 3, &ctlpointsBack[0][0][0],
                        4, 4, GL_MAP2_NORMAL);
    gluEndSurface(theNurb);

    //draw control points
    /*glPointSize(5.0);
    //glDisable(GL_LIGHTING);
    glColor3f(1.0, 1.0, 0.5);
    glBegin(GL_POINTS);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                glVertex3f(ctlpointsBack[i][j][0],
                           ctlpointsBack[i][j][1], ctlpointsBack[i][j][2]);
            }
        }
    glEnd();*/
}

void Superficie::drawWireSphere(GLdouble radius, GLint slices, GLint stacks)
{
        gluQuadricDrawStyle(quadObj, GLU_LINE);
        gluQuadricNormals(quadObj, GLU_SMOOTH);
        gluSphere(quadObj, radius, slices, stacks);
}

void Superficie::drawSolidSphere(GLdouble radius, GLint slices, GLint stacks)
{
        gluQuadricDrawStyle(quadObj, GLU_FILL);
        gluQuadricNormals(quadObj, GLU_SMOOTH);
        gluSphere(quadObj, radius, slices, stacks);
}

void Superficie::doughnut(GLfloat r, GLfloat R, GLint nsides, GLint rings)
{
        int i, j;
        GLfloat theta, phi, theta1;
        GLfloat cosTheta, sinTheta;
        GLfloat cosTheta1, sinTheta1;
        GLfloat ringDelta, sideDelta;

        ringDelta = 2.0 * M_PI / rings;
        sideDelta = 2.0 * M_PI / nsides;

        theta = 0.0;
        cosTheta = 1.0;
        sinTheta = 0.0;
        for (i = rings - 1; i >= 0; i--) {
                theta1 = theta + ringDelta;
                cosTheta1 = cos(theta1);
                sinTheta1 = sin(theta1);
                glBegin(GL_QUAD_STRIP);
                phi = 0.0;
                for (j = nsides; j >= 0; j--) {
                        GLfloat cosPhi, sinPhi, dist;

                        phi += sideDelta;
                        cosPhi = cos(phi);
                        sinPhi = sin(phi);
                        dist = R + r * cosPhi;

                        glNormal3f(cosTheta1 * cosPhi, -sinTheta1 * cosPhi, sinPhi);
                        glVertex3f(cosTheta1 * dist, -sinTheta1 * dist, r * sinPhi);
                        glNormal3f(cosTheta * cosPhi, -sinTheta * cosPhi, sinPhi);
                        glVertex3f(cosTheta * dist, -sinTheta * dist,  r * sinPhi);
                }
                glEnd();
                theta = theta1;
                cosTheta = cosTheta1;
                sinTheta = sinTheta1;
        }
}

void Superficie::drawWireTorus(GLdouble innerRadius, GLdouble outerRadius,
        GLint nsides, GLint rings)
{
        glPushAttrib(GL_POLYGON_BIT);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        doughnut(innerRadius, outerRadius, nsides, rings);
        glPopAttrib();
}

void Superficie::drawSolidTorus(GLdouble innerRadius, GLdouble outerRadius,
        GLint nsides, GLint rings)
{
        doughnut(innerRadius, outerRadius, nsides, rings);
}

void Superficie::drawWireCilinder(GLfloat bottomRadius, GLfloat topRadius, GLfloat length,GLint nsides, GLint rings)
{
    glLineWidth(4.0);
    gluQuadricDrawStyle(quadObj, GLU_LINE);
    gluQuadricNormals(quadObj, GLU_SMOOTH);
    gluCylinder(quadObj,bottomRadius,topRadius,length,nsides,rings);
}

void Superficie::drawSolidCilinder(GLfloat bottomRadius, GLfloat topRadius, GLfloat length,GLint nsides, GLint rings)
{

    gluQuadricDrawStyle(quadObj, GLU_FILL);
    gluQuadricNormals(quadObj, GLU_SMOOTH);
    gluCylinder(quadObj,bottomRadius,topRadius,length,nsides,rings);
}

void Superficie::drawWireSurface()
{
    gluNurbsProperty(theNurb, GLU_DISPLAY_MODE, GLU_OUTLINE_POLYGON);
    drawSurfaceFront();
    drawSurfaceBack();
}

void Superficie::drawSolidSurface()
{
    gluNurbsProperty(theNurb, GLU_DISPLAY_MODE, GLU_FILL);
    drawSurfaceFront();
    drawSurfaceBack();
}

void Superficie::drawBox(GLfloat size, GLenum type)
{
        static GLfloat n[6][3] = {
                {-1.0, 0.0, 0.0},
                {0.0, 1.0, 0.0},
                {1.0, 0.0, 0.0},
                {0.0, -1.0, 0.0},
                {0.0, 0.0, 1.0},
                {0.0, 0.0, -1.0}
        };
        static GLint faces[6][4] = {
                {0, 1, 2, 3},
                {3, 2, 6, 7},
                {7, 6, 5, 4},
                {4, 5, 1, 0},
                {5, 6, 2, 1},
                {7, 4, 0, 3}
        };
        GLfloat v[8][3];
        GLint i;

        v[0][0] = v[1][0] = v[2][0] = v[3][0] = -size / 2;
        v[4][0] = v[5][0] = v[6][0] = v[7][0] = size / 2;
        v[0][1] = v[1][1] = v[4][1] = v[5][1] = -size / 2;
        v[2][1] = v[3][1] = v[6][1] = v[7][1] = size / 2;
        v[0][2] = v[3][2] = v[4][2] = v[7][2] = -size / 2;
        v[1][2] = v[2][2] = v[5][2] = v[6][2] = size / 2;

        for (i = 5; i >= 0; i--) {
                glBegin(type);
                glNormal3fv(&n[i][0]);
                glVertex3fv(&v[faces[i][0]][0]);
                glVertex3fv(&v[faces[i][1]][0]);
                glVertex3fv(&v[faces[i][2]][0]);
                glVertex3fv(&v[faces[i][3]][0]);
                glEnd();
        }
}

void Superficie::drawWireCube(GLdouble size)
{
        drawBox(size, GL_LINE_LOOP);
}

void Superficie::drawSolidCube(GLdouble size)
{
        drawBox(size, GL_QUADS);
}

void Superficie::drawSpace()
{

    //techo, piso, pared

    glBindTexture(GL_TEXTURE_2D, texture[2]);
    glBegin(GL_QUADS);
    // Pared fondo
        glNormal3f(0.0,0.0,1.0);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(-10.0f,-7.0f, -10.0f);
        glTexCoord2f(1.0f, 0.0f); glVertex3f(10.0f,-7.0f,-10.0f);
        glTexCoord2f(1.0f, 1.0f); glVertex3f(10.0f, 13.0f,-10.0f);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(-10.0f, 13.0f, -10.0f);
    // Pared izq
        glNormal3f(1.0,0.0,0.0);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(-10.0f,-7.0f, 20.0f);
        glTexCoord2f(1.0f, 0.0f); glVertex3f(-10.0f,-7.0f,-10.0f);
        glTexCoord2f(1.0f, 1.0f); glVertex3f(-10.0f, 13.0f,-10.0f);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(-10.0f, 13.0f, 20.0f);
    // Pared der
        glNormal3f(-1.0,0.0,0.0);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(10.0f, 13.0f, -10.0f);
        glTexCoord2f(1.0f, 0.0f); glVertex3f(10.0f, -7.0f, -10.0f);
        glTexCoord2f(1.0f, 1.0f); glVertex3f(10.0f, -7.0f,20.0f);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(10.0f, 13.0f, 20.0f);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, texture[1]);
    glBegin(GL_QUADS);
    // Piso
        glNormal3f(0.0,1.0, 0.0);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(-10.0f,-7.0f, 20.0f);
        glTexCoord2f(1.0f, 0.0f); glVertex3f(10.0f,-7.0f,20.0f);
        glTexCoord2f(1.0f, 1.0f); glVertex3f(10.0f, -7.0f,-10.0f);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(-10.0f, -7.0f, -10.0f);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, texture[0]);
    glBegin(GL_QUADS);
    // Techo
        glNormal3f(0.0,-1.0,0.0);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(-10.0f,13.0f, -10.0f);
        glTexCoord2f(1.0f, 0.0f); glVertex3f(-10.0f,13.0f,20.0f);
        glTexCoord2f(1.0f, 1.0f); glVertex3f(10.0f, 13.0f,20.0f);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(10.0f, 13.0f, -10.0f);
    glEnd();
}

void Superficie::loadTextures()
{
    QImage techo, piso, pared;
    QImage techoI,pisoI,paredI;

    if ( !techoI.load( ":/images/techo1.bmp" ) )
    {
        techoI = QImage( 16, 16, QImage::Format_RGB32);
    }if(!pisoI.load(":/images/piso1.jpg"))
    {
        pisoI = QImage( 16, 16, QImage::Format_RGB32 );
    }if(!paredI.load(":/images/pared1.jpg"))
    {
        paredI = QImage( 16, 16, QImage::Format_RGB32 );
    }

    techo = QGLWidget::convertToGLFormat(techoI);
    piso = QGLWidget::convertToGLFormat(pisoI);
    pared = QGLWidget::convertToGLFormat(paredI);

    QImage texturasArray[3];
    texturasArray[0]=techo;
    texturasArray[1]=piso;
    texturasArray[2]=pared;

    texture[0]= bindTexture(techo,GL_TEXTURE_2D);
    texture[1]= bindTexture(piso, GL_TEXTURE_2D);
    texture[2]= bindTexture(pared, GL_TEXTURE_2D);

    glGenTextures( 3, &texture[0] );
    for(int i=0; i<3; i++)
    {
        glBindTexture(GL_TEXTURE_2D, texture[i]);
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexImage2D( GL_TEXTURE_2D, 0, 3, texturasArray[i].width(),
                     texturasArray[i].height(), 0, GL_RGBA,
                     GL_UNSIGNED_BYTE, texturasArray[i].bits() );
    }
}

void Superficie::mousePressEvent(QMouseEvent *event)
{
    switch (event->button()) {
            case Qt::LeftButton:
                    Lx += 20.0f;
                    updateGL();
                    break;

            case Qt::MidButton:
            case Qt::RightButton:
                    Lx -= 20.0f;
                    updateGL();
                    break;
            default:
                    break;
    }
}

/*GLfloat* Superficie::normalPlane(GLfloat x1, GLfloat y1, GLfloat z1,
                                 GLfloat x2, GLfloat y2, GLfloat z2,
                                 GLfloat x3, GLfloat y3, GLfloat z3)
{
    GLfloat normal[3];
    normal[0]=((y2-y1)*(z3-z1))-((z2-z1)*(y3-y1));
    normal[1]=((x2-x1)*(z3-z1))-((z2-z1)*(x3-x1));
    normal[2]=((x2-x1)*(y3-y1))-((y2-y1)*(x3-x1));
    GLfloat* normalp= normal;
    return normalp;
}*/

